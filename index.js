/*créez une fonction qui prends deux nombres comme arguments 
  et retourne leur somme*/

function addition(a, b) {
    return a + b;
}

/* ecrivez une fonction qui prends un nombre entier de minutes et le convertit en secondes(convertir minutes en secondes)
   convertir(5)=300 . convertir(3)=180 . convertir(2)=120 */

function conversion(minutes) {
    return minutes * 60;
}

/* créez une fonction qui prend la base et la hauteur d'un triangle et renvoie sa surface.(l'air d'un triangle)
   Exemple : triArea(7,4) = 14 . triArea(10,10)=50 . notes: l'aie d'un triangle est (base*hauteur)/2  */

function triArea(base, height) {
    return (base * height) / 2;

}

/*creez une fonction qui prend l'age en année et renvoie l'age en jours .
 Exemple : calcAge(20)=7300 . calcAge(65)= 23725  */

function calcAge(age) {
    return age * 365;
}
/* créeez une fonction qui prend et retourne la puissance calculer. voltage*current */

function circuitPower(voltage, current) {
    return voltage * current;
}

/* Corrigez le code dans l’onglet code pour réussir ce défi (uniquement les erreurs de syntaxe).
 Regardez les exemples ci-dessous pour avoir une idée de ce que la fonction devrait faire. */

function cube(a) {
    return a ** 3;
}

/* Créez une fonction qui prend un nombre comme seul argument 
et renvoie s’il est inférieur ou égal à zéro, sinon renvoie .true or false  */

function compare(n) {
    if (n <= 0) {
        return true;
    } else {


        return false;
    }

}

/* Étant donné deux nombres, retournez si la somme des deux nombres est inférieure à 100. Sinon, retournez .true or false

    Exemples : lessThan100(22, 15) ➞ true (2 + 15 = 37) .lessThan100(83, 34) ➞ false (83 + 34 = 117) */

function lessThan100(a, b) {
    if ((a + b) < 100) {
        return true;
    } else {

        return false
    }
}

/* Vous comptez les points pour un match de basket-ball, compte tenu du nombre de 2 points marqués et de 3 points marqués,
 trouvez les derniers points pour l’équipe et retournez cette valeur. 
 Exemples : points(1, 1) ➞ 5   points(38, 8) ➞ 100  */


function points(twoPointers, threePointers) {



}

/* Écrivez une fonction qui convertit un objet en tableau, où chaque élément représente une paire clé-valeur sous la forme d’un tableau.
 exemple : toArray({ a: 1, b: 2 }) ➞ [["a", 1], ["b", 2]]*/

function convertionObjectEnTableau(monobject) {
    return Object.entries(monobject);
}

const data = convertionObjectEnTableau({ a: 1, b: 2 });
console.log(data)

/* Créez une fonction qui renvoie le nombre de valeurs contenues dans un tableau.true */

function valConTable(mots) {
    return mots.filter(mot => mot === true);
}

const data1 = valConTable([true, false, true, false, true])
console.log(data1.length)

/* Un tétraèdre est une pyramide avec une base triangulaire et trois côtés. 
Un nombre tétraédrique est un nombre d’éléments dans un tétraèdre.

Créez une fonction qui prend un entier et retourne le ème nombre tétraédrique.n n */

function tetraNumber(n) {

    return (n * (n + 1) * (n + 2)) / 6;

}
const data = tetraNumber(n)

console.log(data);

/* ecrire un programme qui demande a 10 personnes d'entree leur : nom , prenom , email ,ville et quartier
chaque fois qu'une personne entre ses informations 
creez un objet personne qui stockera les informations stocker par l'utilisateur puis ensuite les ajouter a un tableau d'objet personne
une fois les 10 personnesatteintes , afficher dans une console.
NB : la ville et le quartier de chaque personne est regrouper sous un objet addresse dans l'objet personne. */



function saisir(taille) {
    let personnes = [];
    for (let i = 0; i < taille; i++) {
        let personne = {
            addresse: {}
        };
        personne.nom = prompt("votre nom svp");
        personne.prenom = prompt("votre prenom svp");
        personne.email = prompt("votre email svp");
        personne.addresse.ville = prompt("votre ville svp");
        personne.addresse.quartier = prompt("votre quartier svp");
        personnes.push(personne);


    }
    return personnes;

}

const data = saisir(2);

for (let index in data) {
    let value = data[index];
    if (index === 7 || index === 8) {
        delete value.email;
    }
    console.log("nom: " + value.nom + " prenom: " + value.prenom + " email: " + value.email + " ville: " + value.addresse.ville + " quartier: " + value.addresse.quartier);
}

/* Un tétraèdre est une pyramide avec une base triangulaire et trois côtés. 
Un nombre tétraédrique est un nombre d’éléments dans un tétraèdre.

Créez une fonction qui prend un entier et retourne le ème nombre tétraédrique.n n */

function tetraNumber(n) {

    return (n * (n + 1) * (n + 2)) / 6;

}
const data = tetraNumber(n)

console.log(data);